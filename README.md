# i.MXMP Camera Test scripts

## Configuration

Copy ``env-sample.sh`` to ``env.sh`` and initialize the required variables.

```
	$ cp env-sample.sh env.sh
```
