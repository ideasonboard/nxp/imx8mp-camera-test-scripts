#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2021-2022, Ideas on Board Oy
#
# Author: Laurent Pinchart <laurent.pinchart@ideasonboard.com>

set -e

source "$(dirname $0)/helpers.sh"

#
# Parse command line arguments
#

usage() {
	echo "Usage: $0 [options] {input}"
	echo ""
	echo "Capture frames from the camera sensor connected to the given ISI input on the"
	echo "i.MX8MP. If the input isn't specified, input 0 is used."
	echo ""
	echo "Supported options:"
	echo "-b, --buffers value       Number of buffers to use for capture (default: 4)"
	echo "-c, --count value         Number of frames to capture (default: 8)"
	echo "-d, --debug               Enable debugging in the kernel drivers"
	echo "-f, --format format       The output format and size. This option can be"
	echo "                          repeated to capture from multiple pipelines"
	echo "                          concurrently."
	echo "-g, --gain value          Set the sensor gain in 0.3dB increments (0 to 240)"
	echo "-h, --help                Show this help text"
	echo "-i, --input format        The input format and size"
	echo "-t, --test pattern        Select a test pattern (0 to disable the pattern)"
	echo "-x, --exposure value      Set the sensor exposure time in line units (1 to 1123)"
	echo "    --vflip		Vertically flip the frame"
	echo "    --hflip      		Horizontally flip the frame"
	echo "    --no-save             Do not save captured frames to disk"
	echo ""
	echo " format   = 4cc '/' width 'x' height ;"
	echo ""
	echo "where the fields are"
	echo " 4cc             V4L2 pixel format or media bus code 4CC"
	echo " width           Image width in pixels"
	echo " height          Image height in pixels"

}

input=0
buffers=4
count=8
debug=0
exposure=
gain=
in_format=
out_formats=
pattern=0
save=1
vflip=0
hflip=0

while [ $# != 0 ] ; do
	option="$1"
	shift

	case "${option}" in
	-b|--buffers)
		buffers="$1"
		shift
		;;
	-c|--count)
		count="$1"
		shift
		;;
	-d|--debug)
		debug=1
		;;
	-f|--format)
		out_formats="${out_formats} $1"
		shift
		;;
	-g|--gain)
		gain="$1"
		shift
		;;
	-h|--help)
		usage
		exit 0
		;;
	-i|--input)
		in_format="$1"
		shift
		;;
	-t|--test)
		pattern="$1"
		shift
		;;
	-x|--exposure)
		exposure="$1"
		shift
		;;
	--vflip)
		vflip=1
		;;
	--hflip)
		hflip=1
		;;
	--no-save)
		save=
		;;
	[0-9])
		input="${option}"
		;;
	*)
		echo "Unknown option ${option}"
		exit 1
		;;
	esac
done

#
# Initialize the pipeline
#
isi_pipeline_init $input
isi_debug_enable $debug

#
# Pick a suitable format based on the sensor
#
sensor=$(isi_sensor_model)

case "${sensor}" in
	imx290)
		in_code="SRGGB10_1X10"
		in_size="1920x1080"
		def_format="SRGGB10"
		;;
	imx296)
		in_code="Y10_1X10"
		in_size="1456x1088"
		def_format="Y10"
		;;
	ov5640)
		in_code="UYVY8_1X16"
		in_size="2592x1944"
		def_format="YUYV"
		;;
	ov5647)
		in_code="SBGGR10_1X10"
		in_size="1920x1080"
		def_format="SBGGR10"
		;;
	*)
		echo "Unknown sensor ${sensor}"
		exit 1
		;;
esac

if [ -n "${in_format}" ] ; then
	in_code=${in_format%/*}
	in_size=${in_format#*/}
fi

out_formats="${out_formats:-${def_format}/${in_size}}"
out_mbus_formats=
out_pix_formats=

for out_format in ${out_formats} ; do
	out_size=${out_format#*/}
	out_format=${out_format%/*}

	out_code=$(isi_capture_pix_to_mbus ${out_format})

	out_mbus_formats="${out_mbus_formats} ${out_code}/${out_size}"
	out_pix_formats="${out_pix_formats} ${out_format}"
done

#
# Configure the pipeline
#
isi_pipeline_config $input $in_code/$in_size $out_mbus_formats

#
# Set controls
#

if [ ! -z "$exposure" ] ; then
	isi_sensor_set_control 0x00980911 ${exposure}
fi

if [ ! -z "$gain" ] ; then
	isi_sensor_set_control 0x00980913 ${gain}
fi

isi_sensor_set_control 0x009f0903 ${pattern}

isi_set_hflip 0 $hflip
isi_set_vflip 0 $vflip

#
# Capture frames
#

if [ "$count" = 0 ] ; then
	format=$(array_element 0 ${out_pix_formats})
	size=$(array_element 0 ${out_mbus_formats})
	size=${size#*/}

	echo "Pipeline configured, use $(isi_capture_device 0) to capture ${format} ${size}"
	exit 0
fi

if [ ! -z $save ] ; then
	count="${count}/0"
fi

isi_capture ${out_pix_formats} ${count} -n ${buffers}

isi_capture_finalize "./"
