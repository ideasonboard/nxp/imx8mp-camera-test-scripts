#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2021-2022, Ideas on Board Oy
#
# Author: Laurent Pinchart <laurent.pinchart@ideasonboard.com>

set -e

source "$(dirname $0)/env.sh"

#
# Miscellaneous helper functions
#

# Return the n'th element of an array
# $1: Element index
# $2..: Array elements
array_element() {
	shift $(($1+1))
	echo $1
}

# Check if file exists
# $1..: File names
#
# This function check if the file given in the first argument exists. It allows
# testing if files matching a glob pattern exist, with e.g.
#
#   file_exist /path/to/*.ext
file_exist() {
	test -e "$1"
}

# Check if a string matches a pattern
string_match() {
	echo "$1" | grep -q "$2"
}

#
# Media controller helper functions
#

# Return the index of the first connected source pad of an entity
# $1: The entity
mc_entity_first_source() {
	local entity="$1"

	${mediactl} -p | awk '
/^- entity / {
	in_entity=0
}

/^- entity [0-9]+: '${entity}' / {
	in_entity=1
}

tolower($0) ~ /^[ \t]+pad[0-9]+: source/ {
	pad=gensub(/^[ \t]+pad([0-9]+):.*/, "\\1", "g")
}

/^[ \t]+-> "[^"]+"/ {
	if (in_entity) {
		print pad
		exit
	}
}'
}

# Return the pad connected to a given pad
# $1: The pad, expressed as "entity":index
mc_remote_pad() {
	local entity="${1%:*}"
	local pad="${1#*:}"

	${mediactl} -p | awk '
/^- entity / {
	in_entity=0
}

/^- entity [0-9]+: '${entity}' / {
	in_entity=1
}

/^[ \t]+pad/ {
	in_pad=0
}

/^[ \t]+pad'${pad}': / {
	in_pad=1
}

/^[ \t]+(<-|->) "[^"]+"/ {
	if (in_entity && in_pad) {
		print gensub(/^[^"]+"([^"]+)":([0-9]+).*$/, "\\1:\\2", "g")
		exit
	}
}'
}

# Return the entity connected to a given pad
# $1: The pad, expressed as "entity":index
mc_remote_entity() {
	local pad=$(mc_remote_pad "$1")

	echo ${pad%:*}
}

mc_reset() {
    ${mediactl} -r
}

#
# ISI helper functions
#

# Traverse the media controller graph to find relevant entities
#
# Input args:
# - $1 = input: The ISI input to capture from

__isi_sensor=
__isi_csis=
__isi_xbar="crossbar"
__isi_capture_formats=
__isi_capture_sizes=

isi_pipeline_init() {
	local input=${1}
	__isi_csis="$(mc_remote_entity ${__isi_xbar}:${input})"

	if [ -z "${__isi_csis}" ] || ! string_match "${__isi_csis}" "[0-9]*\.csi$" ; then
		echo "Invalid input ${input}"
		exit 1
	fi

	__isi_sensor="$(mc_remote_entity ${__isi_csis}:0)"

	if [ -z "${__isi_sensor}" ] ; then
		echo "No sensor connected to input ${input}"
		exit 1
	fi
}

# Enable debugging in kernel drivers for devices in the pipeline
#
# Input args:
# - $1 = debug: 0 to disable debug, 1 to enable 1
isi_debug_enable() {
	if [ ! -f /sys/kernel/debug/debug_enabled ] ; then
		mount -t debugfs debugfs /sys/kernel/debug
	fi

	if [ ${debug} -gt 0 ] ; then
		echo Y > "/sys/kernel/debug/${__isi_csis/csis-/}/debug_enable"

		echo "file imx8-isi-hw.c +p"  > /sys/kernel/debug/dynamic_debug/control
		echo "file imx8-isi-pipe.c +p"  > /sys/kernel/debug/dynamic_debug/control
		echo "file imx8-isi-video.c +p"  > /sys/kernel/debug/dynamic_debug/control
		echo "file v4l2-subdev.c +p"  > /sys/kernel/debug/dynamic_debug/control
	else
		echo N > "/sys/kernel/debug/${__isi_csis/csis-/}/debug_enable"

		echo "file imx8-isi-hw.c -p"  > /sys/kernel/debug/dynamic_debug/control
		echo "file imx8-isi-pipe.c -p"  > /sys/kernel/debug/dynamic_debug/control
		echo "file imx8-isi-video.c -p"  > /sys/kernel/debug/dynamic_debug/control
		echo "file v4l2-subdev.c -p"  > /sys/kernel/debug/dynamic_debug/control
	fi
}

# Retrieve the model of the sensor connected to the pipeline input
isi_sensor_model() {
	echo "${__isi_sensor% *}"
}

# Set a V4L2 control for the camera sensor
#
# Input args:
# - $1 = ctrl: The control ID as a hex value
# - $2 = value: The control numerical value
isi_sensor_set_control() {
	local ctrl=$1
	local value=$2

	local sdev=$(${mediactl} -e "${__isi_sensor}")

	${yavta} --no-query -w "${ctrl} ${value}" ${sdev}
}

# Configure the capture pipeline with format and sizes
#
# Input args:
# - $1 = input: The ISI input to capture from
# - $2 = in_format: Format of the ISI input
# - $3.. = out_formats: Formats for the ISI outputs
#
# The input and output formats are expressed as
#
# in_format = code '/' width 'x' height
# out_format = code '/' width 'x' height [ '+' crop ]
# crop = '(' left ',' top ')/' width 'x' height
#
# Multiple output formats can be specified, to capture multiple streams (with
# different or identical configurations) from the same input.
#
isi_pipeline_config() {
	local input=$1
	local in_code=${2%/*}
	local in_size=${2#*/}
	shift 2

	local sensor_src_pad="$(mc_entity_first_source ${__isi_sensor})"
	local xbar_first_src_pad="$(mc_entity_first_source ${__isi_xbar})"

	__isi_capture_formats=
	__isi_capture_sizes=

	mc_reset

	${mediactl} -l "'${__isi_sensor}':${sensor_src_pad} -> '${__isi_csis}':0 [1]"
	${mediactl} -l "'${__isi_csis}':1 -> '${__isi_xbar}':${input} [1]"

	local num_pipes=$#
	local routes=

	echo "Capture pipeline: [${__isi_sensor} |0] -> [0| ${__isi_csis} |1] ->"

	for i in $(seq 0 $((num_pipes-1))) ; do
		local pad=$((xbar_first_src_pad+i))
		local pipe="mxc_isi.${i}"

		echo "        -> [${input}| ${__isi_xbar} |${pad}] -> [0| ${pipe} |1] -> [0|${pipe}.capture]"

		${mediactl} -l "'${__isi_xbar}':${pad} -> '${pipe}':0 [1]"
		${mediactl} -l "'${pipe}':1 -> '${pipe}.capture':0 [1]"

		if [ -n "$routes" ] ; then
			routes="$routes, ${input}/0 -> $((pad))/0 [1]"
		else
			routes="${input}/0 -> $((pad))/0 [1]"
		fi
	done

	${mediactl} -R "'${__isi_xbar}' [${routes}]"

	${mediactl} -V "'${__isi_sensor}':${sensor_src_pad} [fmt:${in_code}/${in_size} field:none]"
	${mediactl} -V "'${__isi_csis}':1 [fmt:${in_code}/${in_size} field:none]"

	for i in $(seq 0 $((num_pipes-1))) ; do
		local pad=$((xbar_first_src_pad+i))
		local pipe="mxc_isi.${i}"
		local out_format=${1%+*}
		local out_crop=${1#*+}
		shift

		if [ ${out_format} == ${out_crop} ] ; then
			out_crop=
		fi

		local out_code=${out_format%/*}
		local out_size=${out_format#*/}

		${mediactl} -V "'${__isi_xbar}':${pad} [fmt:${in_code}/${in_size} field:none]"
		${mediactl} -V "'${pipe}':0 [compose:(0,0)/${out_size}]"

		if [ ! -z $out_crop ]; then
			out_size=${out_crop#*/}
			${mediactl} -V "'${pipe}':1 [crop:$out_crop]"
		fi

		${mediactl} -V "'${pipe}':1 [fmt:${out_code}/${out_size} field:none]"

		__isi_capture_sizes="${__isi_capture_sizes} ${out_size}"
	done

	${mediactl} -V "'${pipe}':1 [fmt:${out_code}/${out_size} field:none]"
}

# Enable or disable horizontal flipping on the output of an ISI pipeline
#
# Input args:
# - $1 = pipe: Index of the ISI pipeline
# - $2 = flip: 0 to disable flipping, 1 to enable it
isi_set_hflip() {
	local pipe=$1
	local flip=$2

	local vdev=$(isi_capture_device ${pipe})

	${yavta} --no-query -w "0x00980914 ${flip}" ${vdev}
}

# Enable or disable vertical flipping on the output of an ISI pipeline
#
# Input args:
# - $1 = pipe: Index of the ISI pipeline
# - $2 = flip: 0 to disable flipping, 1 to enable it
isi_set_vflip() {
	local pipe=$1
	local flip=$2

	local vdev=$(isi_capture_device ${pipe})

	${yavta} --no-query -w "0x00980915 ${flip}" ${vdev}
}

# Get the V4L2 capture device at the output of an ISI pipeline
#
# Input args:
# - $1 = pipe: Index of the ISI pipeline
isi_capture_device() {
	local pipe=$1

	${mediactl} -e "mxc_isi.${pipe}.capture"
}

# Convert a V4L2 pixel format to a media bus code for an ISI capture interface
#
# Input args:
# - $1 = pixfmt: The V4L2 pixel format
isi_capture_pix_to_mbus() {
	local pixfmt=${1}

	case ${pixfmt} in
	YUYV|YUVA32|NV12|NV12M|NV16|NV16M|YUV444M)
		echo YUV8_1X24
		;;
	RGB565|RGB24|BGR24|XBGR32|ABGR32)
		echo RGB888_1X24
		;;
	GREY)
		echo Y8_1X8
		;;
	SBGGR8|SGBRG8|SGRBG8|SRGGB8)
		echo ${pixfmt}_1X8
		;;
	SBGGR10|SGBRG10|SGRBG10|SRGGB10|Y10)
		echo ${pixfmt}_1X10
		;;
	SBGGR12|SGBRG12|SGRBG12|SRGGB12|Y12)
		echo ${pixfmt}_1X12
		;;
	SBGGR14|SGBRG14|SGRBG14|SRGGB14|Y14)
		echo ${pixfmt}_1X14
		;;
	MJPEG)
		echo JPEG_1X8
		;;
	esac
}

# Capture frames from an ISI pipeline
#
# Input args:
# - $1 = pipe: Index of the ISI pipeline
# - $2 = format: Pixel format
# - $3 = count: Number of frames to capture and skip (see isi_capture)
# - $4.. : Additional capture arguments, passed unmodified to yavta
isi_capture_one() {
	local pipe=$1
	local format=$2
	local count=${3%/*}
	local skip=0
	local save

	if [ ${count} != ${3} ] ; then
		skip=${3#*/}
		save="--file=/tmp/frame-pipe${pipe}-#.bin"
	fi

	shift 3

	if [ ${count} == 0 ] ; then
		count=
	fi

	local size=$(array_element ${pipe} ${__isi_capture_sizes})
	local vdev=$(isi_capture_device ${pipe})

	${yavta} -f ${format} -s ${size} -c${count} --skip=${skip} ${save} $@ ${vdev}
}

# Capture frames from all configured ISI pipelines
#
# Input args:
# - $1..n = formats: Pixel formats, one per configured pipeline
# - $n+1 = count: Number of frames to capture and skip
# - $n+2.. : Additional capture arguments, passed unmodified to yavta
#
# The count argument is expressed as
#
# count [ '/' skip ]
#
# 'count' indicates the number of frames to capture (0 for infinite capture).
# If a 'skip' value is specified, it enables saving frames to disk and tells
# how many frames to skip after capture start before starting saving.
isi_capture() {
	local num_pipes=0
	local i

	rm -f /tmp/frame-pipe*.bin

	for i in ${__isi_capture_sizes} ; do
		__isi_capture_formats="${__isi_capture_formats} $1"
		num_pipes=$((num_pipes+1))
		shift
	done

	local format
	local pipe=0

	for format in ${__isi_capture_formats} ; do
		# Run all pipelines asynchronously, the isi_capture_finalize()
		# function will wait for them to complete.
		isi_capture_one ${pipe} ${format} $@ &
		pipe=$((pipe+1))
	done
}

# Finalize a capture cycle by converting and moving frames to output directory
#
# Input args:
# - $1 = outdir: Path to the output directory where to store frames
isi_capture_finalize() {
	local outdir=$1
	local format
	local pipe=0

	wait

	for format in ${__isi_capture_formats} ; do
		local size=$(array_element $pipe ${__isi_capture_sizes})

		if file_exist /tmp/frame-pipe${pipe}-0* ; then
			r2r_frame_convert "frame-pipe${pipe}" '/tmp/' ${format} ${size} "${outdir}"
			mv /tmp/frame-pipe${pipe}-0*.bin "${outdir}"
		fi

		pipe=$((pipe+1))
	done
}

# Convert binary frames into .ppm format for easier inspection
#
# Input args:
# - $1 = prefix: The frame file name prefix
# - $1 = indir: Path to the directory with frames in .bin formats
# - $2 = format: The pixel format used for capture
# - $3 = size: The frame size
# - $4 = outdir: Path where to store the final image in .ppm format
r2r_frame_convert() {
	if [ -z ${r2r} ] ; then
		return
	fi

	local prefix=${1}
	local indir=${2}
	local fmt=${3}
	local size=${4}
	local outdir=${5}

	if [ $fmt == "MJPEG" ]; then
		return
	fi

	case $fmt in
	[AX]BGR32)
		fmt=BGR32
		;;
	[AX]RGB32)
		fmt=RGB32
		;;
	Y[UV][UV]4[124][0124]M)
		fmt=${fmt%M}P
		;;
	NV[1246][1246]M)
		fmt=${fmt%M}
		;;
	esac

	for f in ${indir}/${prefix}-0*.bin; do
		local name=$(basename $f .bin)
		${r2r} -f $fmt -s $size $indir/$name.bin $outdir/$name.ppm
	done
}
