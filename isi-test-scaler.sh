#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2022 Jacopo Mondi <jacopo@jmondi.org>

#
# Test the ISI scaler by downscaling 1920x1080 in both UYVY and RGB565_1X16
#

set -e

source "$(dirname $0)/helpers.sh"

usage() {
	echo "$0: Test ISI scaler by downscaling 1920x1080 with an optional crop rectangle"
	echo ""
	echo " -c, --crop rect	Crop rectangle to apply after downscaling"
	echo "			The crop rectangle is only applied if smaller than the compose one"
	echo " -d, --debug	Enable debug"
	echo " -h, --help	Show this help message"
	echo ""
	echo "rect	= '(' left ',' top, ')' '/' size ;"
	echo "size      = width 'x' height ;"
	echo ""
	echo "where the fields are"
	echo " top             Crop rectangle vertical offset from compose rectangle in pixels"
	echo " left            Crop rectangle horizontal offset from compose rectangle in pixels"
	echo " width           Crop rectangle width in pixels"
	echo " height          Crop rectangle height in pixels"
}

debug=0
crop=""
while [ $# != 0 ] ; do
	option="$1"
	shift

	case ${option} in
	-c|--crop)
		crop=$1
		shift
		;;
	-d|--debug)
		debug=1
		;;
	-h|--help)
		usage
		exit 0
		;;
	esac
done

input=0
isi_pipeline_init $input
isi_debug_enable $debug

out="./scaler_test"

#
# Run a single capture session testing the ISI scaler
#
# Input args:
# - $1 = input: The ISI pipe instance index
# - $2 = code: The media bus code to capture
# - $3 = pixfmt: The image pixelformat
# - $4 = insize: The image size before the scaler
# - $5 = outsize: The image size after the scaler
#
isi_scaler_capture_single() {
	local input=${1}
	local code=${2}
	local pixfmt=${3}
	local insize=${4}
	local outsize=${5}
	local outdir=${out}/${code}/${insize}_${outsize}/

	# remove stale files or create output directory
	if [ -d ${outdir} ] ; then
		rm  -rf ${outdir}/*
	else
		mkdir -p ${outdir}
	fi

	isi_pipeline_config $input $code/$insize $code/$outsize+$crop

	if [ ! -z $crop ]; then
		outsize=${crop#*/}
	fi

	isi_capture ${pixfmt} 10/7

	isi_capture_finalize "${outdir}"
}

formats="UYVY/YUYV RGB565_1X16/RGB24"

for format in ${formats}; do
	code=${format%/*}
	pixfmt=${format#*/}

	insize="1920x1080"
	outsizes="1280x720 1024x768 720x480 640x480 320x240 160x120"

	for outsize in ${outsizes}; do
		# Skip configuration where the source crop rectangle is larger
		# than the sink compose rectangle
		skip=0
		if [ ! -z $crop ]; then
			outw=${outsize%x*}
			outh=${outsize#*x}
			cropsize=${crop#*/}
			cropw=${cropsize%x*}
			croph=${cropsize#*x}

			if [ $outw -le $cropw ] || [ $outh -le $croph ]; then
				skip=1;
			fi
		fi

		if [ $skip -eq 0 ]; then
			isi_scaler_capture_single 0 $code $pixfmt $insize $outsize
		fi
	done
done
