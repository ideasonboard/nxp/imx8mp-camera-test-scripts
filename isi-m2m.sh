#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2022, Ideas on Board Oy
#
# Author: Laurent Pinchart <laurent.pinchart@ideasonboard.com>

set -e

source "$(dirname $0)/helpers.sh"

#
# Parse command line arguments
#

usage() {
	echo "Usage: $0 [options] {format} ..."
	echo ""
	echo "Process frames through the ISI M2M element using GStreamer."
	echo ""
	echo "Supported options:"
	echo "-c, --count value         Number of frames to capture (default: 10)"
	echo "-h, --help                Show this help text"
	echo "    --no-save             Do not save captured frames to disk"
	echo ""
	echo " format = input '-' output"
	echo " input = 4CC '/' width 'x' height"
	echo " output = 4CC '/' width 'x' height"
	echo ""
	echo " Where the input and output 4CC codes are expressed using the"
	echo " GStreamer format identifiers. Multiple formats can be specified,"
	echo " to run multiple GStreamer pipelines concurrently. If no format is"
	echo " specific, the default is YUY2/640x480-RGB/320x240."
}

count=10
formats=
save=1

while [ $# != 0 ] ; do
	option="$1"
	shift

	case "${option}" in
	-c|--count)
		count="$1"
		shift
		;;
	-h|--help)
		usage
		exit 0
		;;
	--no-save)
		save=
		;;
	-*)
		echo "Unknown option ${option}"
		exit 1
		;;
	*)
		formats="${formats} ${option}"
		;;
	esac
done

if [ -z "${formats}" ] ; then
	formats="YUY2/640x480-RGB/320x240"
fi

rm -rf /tmp/frame-*.bin
rm -rf frame-*.bin

ctx=0

for format in ${formats} ; do
	in_format=${format%-*}
	in_code=${in_format%/*}
	in_size=${in_format#*/}
	in_width=${in_size%x*}
	in_height=${in_size#*x}

	out_format=${format#*-}
	out_code=${out_format%/*}
	out_size=${out_format#*/}
	out_width=${out_size%x*}
	out_height=${out_size#*x}

	if [ ! -z $save ] ; then
		sink="multifilesink location=/tmp/frame-ctx${ctx}-0%d.bin"
	else
		sink="fakesink"
	fi

	gst-launch-1.0 \
		videotestsrc num-buffers=${count} horizontal-speed=16 ! \
		video/x-raw,format=${in_code},width=${in_width},height=${in_height},framerate=30/1,interlace-mode=progressive ! \
		v4l2convert ! \
		video/x-raw,format=${out_code},width=${out_width},height=${out_height},framerate=30/1,interlace-mode=progressive ! \
		${sink} &

	ctx=$((ctx+1))
done

wait

if [ ! -z $save ] ; then
	ctx=0

	for format in ${formats} ; do
		format=${format#*-}
		size=${format#*/}
		format=${format%/*}

		case $format in
		YUY2)
			format=YUYV
			;;
		RGB)
			format=RGB24
			;;
		esac

		r2r_frame_convert "frame-ctx${ctx}" '/tmp/' ${format} ${size} "./"
		mv /tmp/frame-ctx${ctx}-0*.bin .

		ctx=$((ctx+1))
	done
fi
