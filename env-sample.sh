#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later

# Environment setup

media_dev=/dev/media0

# https://git.linuxtv.org/v4l-utils.git/
media_ctl=/usr/bin/media-ctl
mediactl="${media_ctl} -d ${media_dev}"

# https://git.ideasonboard.org/yavta.git/
yavta=/usr/bin/yavta

# raw2rgbpnm for image conversion (optional)
# https://git.retiisi.eu/?p=~sailus/raw2rgbpnm.git;a=summary
r2r=

if [ ! -x ${yavta} ] ;  then
	echo "yavta executable ${yavta} not found"
	exit 1
fi

if [ ! -x ${media_ctl} ] ;  then
	echo "media-ctl executable ${media_ctl} not found"
	exit 1
fi

if [ ! -c ${media_dev} ] ;  then
	echo "Media device ${media_dev} not present"
	exit 1
fi
