#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2022 Jacopo Mondi <jacopo@jmondi.org>

#
# Test all the supported formats and resolutions of the ov5640 sensor driver
#

set -e

source "$(dirname $0)/helpers.sh"

out="./ov5640_test/"

input=0
isi_pipeline_init $input

capture_test() {
	local size="${1}x${2}"
	local code=$3
	local fmt=$4
	local outdir=$out/$size/$fmt/

	# remove stale files or create output directory
	if [ -d ${outdir} ] ; then
		rm  -rf ${outdir}/*
	else
		mkdir -p ${outdir}
	fi

	isi_pipeline_config $input $code/$size $code/$size

	isi_capture ${fmt} 10/7

	isi_capture_finalize "${outdir}"
}

capture_all_res() {
	local code=$1
	local fmt=$2

	# RAW not supported in lower resolution
	if [ $fmt != "SBGGR8" ] ; then
		capture_test 160 120 $code $fmt
		capture_test 176 144 $code $fmt
		capture_test 320 240 $code $fmt
		capture_test 640 480 $code $fmt
		capture_test 720 480 $code $fmt
		capture_test 720 576 $code $fmt
		capture_test 1024 768 $code $fmt
	fi

	# Full-res mode obtained by cropping do not work with 24-bpp modes
	if [ $fmt == "RGB24" ] ; then
		return
	fi

	capture_test 1280 720  $code $fmt
	capture_test 1920 1080 $code $fmt
	capture_test 2592 1944 $code $fmt
}

capture_all_res UYVY YUYV
capture_all_res RGB565_1X16 RGB565
capture_all_res SBGGR8 SBGGR8
capture_all_res BGR888_1X24 RGB24
capture_all_res JPEG_1X8 MJPEG
