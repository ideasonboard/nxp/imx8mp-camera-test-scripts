#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2022 Jacopo Mondi <jacopo@jmondi.org>

#
# Test the CSC format conversion capabilities
# Test all capture combinations
#

set -e

source "$(dirname $0)/helpers.sh"

usage() {
	echo "$0: Test ISI colorspace conversion (CSC) running multiple captures"
	echo ""
	echo " -d, --debug	Enable debug"
	echo " -h, --help	Show this help message"
	echo " -s, --size	Image size (default is 1280x720)"
}

debug=0
size="1280x720"
while [ $# != 0 ] ; do
	option="$1"
	shift

	case ${option} in
	-d|--debug)
		debug=1
		;;
	-h|--help)
		usage
		exit 0
		;;
	-s|--size)
		size=${1}
		shift
		;;
	esac
done

input=0
isi_pipeline_init $input
isi_debug_enable $debug

out="./csc_test"

#
# Run a single capture session with colorspace conversion in the CSC
#
# Input args:
# - $1 = sensor_code: The sensor produced media bus code
# - $2 = isi_code: The ISI source pad media bus code
# - $3 = pixfmt: The ISI output pixel format
#
csc_capture_single() {
	local sensor_code=${1}
	local isi_code=${2}
	local pixfmt=${3}
	local outdir=${out}/${sensor_code}-${isi_code}-${pixfmt}/

	# remove stale files or create output directory
	if [ -d ${outdir} ] ; then
		rm  -rf ${outdir}/*
	else
		mkdir -p ${outdir}
	fi

	isi_pipeline_config $input $sensor_code/$size $isi_code/$size

	isi_capture ${pixfmt} 10/7

	isi_capture_finalize "${outdir}"
}

#
# Test RGB565_1X16 -> YUV colorspace conversion
#
# Test all the YUV output pixelformat the ISI can produce
#
csc_capture_yuv() {
	pixfmts="YUYV YUVA32 YUV444M NV12 NV12M NV16 NV16M"
	sensor_fmt="RGB565_1X16"
	isi_fmt="YUV8_1X24"

	for pixfmt in ${pixfmts} ; do
		csc_capture_single ${sensor_fmt} ${isi_fmt} ${pixfmt}
	done
}

#
# Test UYVY -> RGB colorspace conversion
#
# Test all the RGB output pixelformat the ISI can produce
#
csc_capture_rgb() {
	pixfmts="RGB565 RGB24"
	sensor_fmt="UYVY"
	isi_fmt="RGB888_1X24"

	for pixfmt in ${pixfmts} ; do
		csc_capture_single ${sensor_fmt} ${isi_fmt} ${pixfmt}
	done

}

csc_capture_yuv
csc_capture_rgb
