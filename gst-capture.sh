#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2021-2022, Ideas on Board Oy
#
# Author: Laurent Pinchart <laurent.pinchart@ideasonboard.com>

set -e

export GST_DEBUG="*:2,libcamerasrc:4"
export GST_PLUGIN_PATH="/usr/local/lib/gstreamer-1.0"
# export GST_REGISTRY_FORK="no"

#
# Parse command line arguments
#

usage() {
	echo "Usage: $0 [options]"
	echo ""
	echo "Capture frames with libcamera using GStreamer."
	echo ""
	echo "Supported options:"
	echo "-c, --camera name         Camera name (default: first available camera)"
	echo "-e, --encode              Encode video stream"
	echo "-f, --format format       Capture format (default: libcamera's default)"
	echo "-h, --help                Print this help text"
	echo "-s, --size size           Capture size (default: libcamera's default)"
	echo "-u, --udp host:port       Stream frames over UDP to host:port"
}

#
# Display
#

kms_stream() {
	gst-launch-1.0 \
		libcamerasrc ${camera_name} ! \
		video/x-raw,${format}${size}colorimetry=bt709,framerate=${capture_frame_rate} ! \
		fpsdisplaysink sync=0 text-overlay=false video-sink=kmssink -v
}

#
# Encoding
#

encode_stream() {
	gst-launch-1.0 \
		libcamerasrc ${camera_name} ! \
		video/x-raw,${format}${size}colorimetry=bt709,framerate=${capture_frame_rate} ! \
		queue ! x264enc ! fakesink # mp4mux ! filesink location=/tmp/video.m4v
}

#
# Network streaming
#

udp_usage() {
	local port=$1

	echo "On the host side, run"
	echo ""
	echo "gst-launch-1.0 \\"
	echo "        udpsrc port=${port} ! \\"
	echo "        application/x-rtp,encoding-name=JPEG,payload=26 ! \\"
	echo "        rtpjpegdepay ! \\"
	echo "        jpegdec ! \\"
	echo "        queue ! \\"
	echo "        autovideosink"
}

udp_stream() {
	local host="${1%:*}"
	local port="${1#*:}"
	local stream_frame_rate="1/1"

	udp_usage ${port}

	gst-launch-1.0 \
		libcamerasrc ${camera_name} ! \
		video/x-raw,${format}${size}colorimetry=bt709,framerate=${capture_frame_rate} ! \
		videorate ! \
		video/x-raw,framerate=${stream_frame_rate} ! \
		jpegenc ! \
		rtpjpegpay ! \
		udpsink host=${host} port=${port}
}

camera_name=
capture_frame_rate="30/1"
encode=
format=
size=
udp_dest=

while [ $# != 0 ] ; do
	option="$1"
	shift

	case "${option}" in
	-c|--camera)
		camera_name="camera-name=$1"
		shift
		;;
	-e|--encode)
		encode=1
		;;
	-f|--format)
		format="format=$1,"
		shift
		;;
	-h|--help)
		usage
		exit 0
		;;
	-s|--size)
		size="$1"
		shift
		;;
	-u|--udp)
		udp_dest="$1"
		shift
		;;
	-*)
		echo "Unknown option ${option}"
		exit 1
		;;
	*)
		host="${option}"
		;;
	esac
done

if [ ! -z ${size} ] ; then
	size="width=${size%x*},height=${size#*x},"
fi

if [ ! -z "${udp_dest}" ] ; then
	udp_stream "${udp_dest}"
elif [ ${encode} ] ; then
	encode_stream
else
	kms_stream
fi
